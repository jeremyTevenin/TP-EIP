package project;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.UUID;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import tp.model.Animal;
import tp.model.Cage;
import tp.model.Center;
import tp.model.Position;

public class Test {

	@org.junit.Test
	public void test() throws JsonParseException, JsonMappingException, IOException {
		Center center = new Center();
		
		Animal anim1 =  new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID());
		Animal anim2 = new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID());
		Animal anim3 = new Animal("Canine", "amazon", "Piranha", UUID.randomUUID());
		Animal anim4 = new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID());
		Animal anim5 = new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID());
		Animal anim6 =  new Animal("De lait", "amazon", "Piranha", UUID.randomUUID());

		Cage usa = new Cage(
				"usa",
				new Position(49.305d, 1.2157357d),
				25,
				new LinkedList<>(Arrays.asList(anim1,anim2))
				);

		Cage amazon = new Cage(
				"amazon",
				new Position(49.305142d, 1.2154067d),
				15,
				new LinkedList<>(Arrays.asList(anim3,anim4,anim5,anim6))
			);

		center.getCages().addAll(Arrays.asList(usa, amazon));
		ObjectMapper mapper = new XmlMapper();
		String result = mapper.writeValueAsString(center);
		mapper.readValue(result, Center.class);
		System.out.println(result);


		String val = "<center><cages><cages><capacity>25</capacity><name>usa</name><position><latitude>49.305</latitude><longitude>1.2157357</longitude></position><residents><cage>usa</cage><id>cfe7ba40-e837-49d7-9888-8fa81f2fd779</id><name>Tic</name><species>Chipmunk</species></residents><residents><cage>usa</cage><id>cad0a644-4f3e-476a-86b7-f32c2ab6192d</id><name>Tac</name><species>Chipmunk</species></residents></cages><cages><capacity>15</capacity><name>amazon</name><position><latitude>49.305142</latitude><longitude>1.2154067</longitude></position><residents><cage>amazon</cage><id>23dfcd83-b921-4ea4-8d02-4e2815be9121</id><name>Canine</name><species>Piranha</species></residents><residents><cage>amazon</cage><id>5e02463c-1e0f-47f9-b0a3-8225bd23fe84</id><name>Incisive</name><species>Piranha</species></residents><residents><cage>amazon</cage><id>03b103a3-7dca-428b-a7d9-ecbef4135302</id><name>Molaire</name><species>Piranha</species></residents><residents><cage>amazon</cage><id>bc816252-b038-4a23-a603-20bb487200bf</id><name>De lait</name><species>Piranha</species></residents></cages></cages><name>Biotropica</name><position><latitude>49.30494</latitude><longitude>1.2170602</longitude></position></center>";
		System.out.println(val);
		mapper.readValue(val, Center.class);
	}
}
