package project;

import java.util.Scanner;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import tp.model.Animal;

public class ProducerConsumer2 {
	public static void main(String[] args) throws Exception {
			// Permet de configurer le logger par défaut
			BasicConfigurator.configure();
			Logger.getRootLogger().setLevel(Level.INFO);
			// Contexte Camel par défaut
			CamelContext context = new DefaultCamelContext();
			Scanner sc = new Scanner(System.in);
			final String result = sc.nextLine();
			// Permet de créer une route contenant le consommateur
			RouteBuilder routeBuilder = new RouteBuilder() {
				
				@Override
				public void configure() throws Exception {
					from("direct:ZooManager")
					.setHeader(Exchange.HTTP_METHOD,constant("GET"))
					.to("http://127.0.0.1:8080/rest-service/zoo-manager/find/byName/" + result)
					.unmarshal().jacksonxml(Animal.class).log("reponse received 2 : ${body.cage}")
					.setBody(simple("${body.cage}"))
					.toD("http://api.geonames.org/search?q=${body}&maxRows=10&username=tp_archi_dist")
					.log("reponse received : ${body}");
				}
			};
			// Permet d'ajouter la route au contexte
			routeBuilder.addRoutesToCamelContext(context);
			// Permet de démarrer le contexte pour activer les routes
			context.start();
			// Permet de créer un producteur
			ProducerTemplate producerTemp = context.createProducerTemplate();
			producerTemp.sendBody("direct:ZooManager", "");
			sc.close();
	}
}
