package project;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.BasicConfigurator;

import java.util.Scanner;

public class ProducerConsumer {
	public static void main(String[] args) throws Exception {
		// Permet de configurer le logger par défaut
		BasicConfigurator.configure();
		// Contexte Camel par défaut
		CamelContext context = new DefaultCamelContext();
		// Permet de créer une route contenant le consommateur
		RouteBuilder routeBuilder = new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				// Permet de définir un consommateur 'consumer-1'
				from("direct:consumer-1").to("log:affiche-1-log");
				from("direct:consumer-2").to("file:messages");
				from("direct:consumer-all")
				.choice()
				.when(header("destinataire").isEqualTo("écrire"))
				.to("direct:consumer-2")
				.otherwise().to("direct:consumer-1");
			}
		};
		// Permet d'ajouter la route au contexte
		routeBuilder.addRoutesToCamelContext(context);
		// Permet de démarrer le contexte pour activer les routes
		context.start();
		// Permet de créer un producteur
		ProducerTemplate producerTemp = context.createProducerTemplate();
		String result = "";
		Scanner sc = new Scanner(System.in);
		while (!(result = sc.nextLine()).equals("exit")) {
			String header = "";
			if (result.charAt(0) == 'w') {
				header = "écrire";
			}
			producerTemp.sendBodyAndHeader("direct:consumer-all", result, "destinataire", header);
		}
		sc.close();
	}
}
