package project;

import java.util.Scanner;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.log4j.BasicConfigurator;

import tp.model.Animal;
import tp.model.Center;

public class ProducerConsumer3 {
	public static void main(String[] args) throws Exception {
		// Permet de configurer le logger par défaut
		BasicConfigurator.configure();
		// Contexte Camel par défaut
		CamelContext context = new DefaultCamelContext();
		// Permet de créer une route contenant le consommateur
		RouteBuilder routeBuilder = new RouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				Processor procAnim = new ProcessorAnimal();
				from("direct:ZooManager")
				.setHeader(Exchange.HTTP_METHOD,constant("GET"))
				.to("http://localhost:8080/rest-service/zoo-manager/animals")
				.convertBodyTo(String.class)
				.unmarshal().jacksonxml(Center.class).process(procAnim)
				.to("http://localhost:8080/rest-service2/zoo-manager/animals")
				.unmarshal().jacksonxml(Center.class).process(procAnim)
				.to("http://localhost:8080/rest-service3/zoo-manager/animals")
				.unmarshal().jacksonxml(Center.class).process(procAnim)
				.log("reponse received : ${body}");
			}
		};
		// Permet d'ajouter la route au contexte
		routeBuilder.addRoutesToCamelContext(context);
		// Permet de démarrer le contexte pour activer les routes
		context.start();
		// Permet de créer un producteur
		ProducerTemplate producerTemp = context.createProducerTemplate();
		producerTemp.sendBody("direct:ZooManager", "");
	}

}
