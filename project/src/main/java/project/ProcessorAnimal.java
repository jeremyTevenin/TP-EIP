package project;

import java.util.Optional;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import tp.model.Center;

public class ProcessorAnimal implements Processor {

	private Center c;
	
	@Override
	public void process(Exchange ex) throws Exception {
		Center center = ex.getIn().getBody(Center.class);
		if (c == null) {
			c = center;
		} else {
			center.getCages().forEach(cage -> {
					cage.getResidents().stream()
						.filter(anim -> c.findAnimalByAnimal(anim) == null)
						.forEach(animal -> c.getCages()
					            .stream()
					            .filter(cage2 -> cage2.getName().equals(animal.getCage()))
					            .findFirst().get()
					            .getResidents()
					            .add(animal)
								);
				});
		}
		ex.getIn().setBody(Center.class);
	}

}
