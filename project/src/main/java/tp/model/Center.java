package tp.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Center {
	
	private Long centerId;

    
	Collection<Cage> cages;
	
    Position position;
	
    String name;

    public Center() {
        cages = new ArrayList<>();
    }

    public Center(Collection<Cage> cages, Position position, String name) {
        this.cages = cages;
        this.position = position;
        this.name = name;
    }

    public Animal findAnimalById(UUID uuid) throws AnimalNotFoundException {
        return this.cages.stream()
                .map(Cage::getResidents)
                .flatMap(Collection::stream)
                .filter(animal -> uuid.equals(animal.getId()))
                .findFirst()
                .orElseThrow(AnimalNotFoundException::new);
    }

    
    public Animal findAnimalByAnimal(Animal animal) {
        return this.cages.stream()
                .map(Cage::getResidents)
                .flatMap(Collection::stream)
                .filter(animal2 -> animal.getCage().equals(animal2.getCage())
                		&& animal.getName().equals(animal2.getName())
                		&& animal.getSpecies().equals(animal2.getSpecies()))
                .findFirst()
                .orElse(null);
    }
    
    public Long getCenterId(){
        return centerId;
    }
    
    public Collection<Cage> getCages() {
        return cages;
    }

    public Position getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }

    public void setCages(Collection<Cage> cages) {
        this.cages = cages;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setName(String name) {
        this.name = name;
    }
}
